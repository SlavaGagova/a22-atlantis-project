﻿using System;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.WaitHelpers;

namespace IpswitchTestFormPage
{
    public class IpswitchTest
    {

        private IWebDriver driver;

        private const String PATH = "https://www.ipswitch.com";

        private const String FIRST_NAME_ID = "Textbox-1";

        private const String LAST_NAME_ID = "Textbox-3";

        private const String EMAIL_ID = "Email-1";

        private const String PHONE_NUMBER_ID = "Textbox-4";

        private const String COMPANY_ID = "Textbox-2";

        private const String PRODUCT_INTEREST = "Dropdown-1";

        private const String COUNTRY = "Country-1";

        private const String CONTACT_US_BUTTON = "//button[@class='Btn Btn--prim -mt3']";

        private const String CONFIRMATION_MESSAGE = "Content_C351_Col00";

        private const String STATE_DROPDOWN = "State-1";

        private const String EXCEEDING_CHARACTER = "tttttttttttttttttttttttttttttttttttttttttttttttttttttttt";

        [SetUp]
        public void StartBrowser()
        {
            driver = new ChromeDriver(".");
            driver.Url = PATH + "/test-form-page";
        }

        [TearDown]
        public void QuitDriver()
        {
            driver.Quit();
        }

        [Test]
        public void TestFill_In_Submission_With_Valid_Input()
        {
            IWebElement elementFirstName = driver.FindElement(By.Id(FIRST_NAME_ID));
            AssertElementDisplayed(elementFirstName);
            elementFirstName.SendKeys("Test");
            Thread.Sleep(500);

            IWebElement elementLastName = driver.FindElement(By.Id(LAST_NAME_ID));
            AssertElementDisplayed(elementLastName);
            elementLastName.SendKeys("Test");
            Thread.Sleep(500);

            IWebElement elementEmail = driver.FindElement(By.Id(EMAIL_ID));
            AssertElementDisplayed(elementEmail);
            elementEmail.SendKeys("test@test.se");

            IWebElement elementPhoneNumber = driver.FindElement(By.Id(PHONE_NUMBER_ID));
            AssertElementDisplayed(elementPhoneNumber);
            elementPhoneNumber.SendKeys("555-555-1234");

            IWebElement elementCompany = driver.FindElement(By.Id(COMPANY_ID));
            AssertElementDisplayed(elementCompany);
            elementCompany.SendKeys("Test");

            IWebElement dropdownProductInterest = driver.FindElement(By.Id(PRODUCT_INTEREST));
            ClickFirstElementInDropdown(dropdownProductInterest, 1);

            IWebElement dropdownCountry = driver.FindElement(By.Id(COUNTRY));
            ClickFirstElementInDropdown(dropdownCountry, 15);

            acceptConsent();

           
            IWebElement elementMessage = driver.FindElement(By.Id("Textarea-1"));
            AssertElementDisplayed(elementMessage);
            elementMessage.SendKeys("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");

            agreeCheckBoxSelected();

            IWebElement elementContactUsButton = driver.FindElement(By.XPath(CONTACT_US_BUTTON));
            AssertElementDisplayed(elementContactUsButton);
            elementContactUsButton.Click();


            IWebElement confirmationMessage = driver.FindElement(By.Id(CONFIRMATION_MESSAGE));
            AssertElementDisplayed(confirmationMessage);

            Assert.AreEqual(driver.Url, "https://www.ipswitch.com/test-form-page");

            Thread.Sleep(10000);    
        }

        [Test]
        public void Test_Form_Submission_With_Empty_Fields()
        {
            acceptConsent();
            IWebElement elementContactUsButton = driver.FindElement(By.XPath(CONTACT_US_BUTTON));
            AssertElementDisplayed(elementContactUsButton);
            elementContactUsButton.Click();
            IWebElement elementFirstName = driver.FindElement(By.Id(FIRST_NAME_ID));
            AssertElementDisplayed(elementFirstName);
            assertElementHasError(elementFirstName, "First Name is Required");
            Thread.Sleep(3000);

        }

        [Test]

        public void Test_State_Apepears_When_Country_Selected_USA()


        {
            IWebElement dropdownCountry = driver.FindElement(By.Id(COUNTRY));
            ClickFirstElementInDropdown(dropdownCountry, 1);
            Thread.Sleep(3000);
            acceptConsent();

            IWebElement dropdownState = driver.FindElement(By.Id(STATE_DROPDOWN));
            AssertElementDisplayed(dropdownState);  

        }

        // this test is supposed to fail as there is a bug. The form allows ti exceed the char.length
        [Test]

        public void Test_FillIng_First_Name_Field_With_More_Than_55_Chars()

        {
            IWebElement elementFirstName = driver.FindElement(By.Id(FIRST_NAME_ID));
            AssertElementDisplayed(elementFirstName);
            elementFirstName.SendKeys(EXCEEDING_CHARACTER);
            Thread.Sleep(500);

            AssertElementDisplayed(elementFirstName);
            assertElementHasError(elementFirstName, "First Name is Required");
            Thread.Sleep(3000);

            acceptConsent();
        }


        [Test]
        public void TestFill_In_Submission_With_Valid_Input_When_Country_USA()
        {
            acceptConsent();
            IWebElement elementFirstName = driver.FindElement(By.Id(FIRST_NAME_ID));
            AssertElementDisplayed(elementFirstName);
            elementFirstName.SendKeys("Test");
            Thread.Sleep(500);

            IWebElement elementLastName = driver.FindElement(By.Id(LAST_NAME_ID));
            AssertElementDisplayed(elementLastName);
            elementLastName.SendKeys("Test");
            Thread.Sleep(500);

            IWebElement elementEmail = driver.FindElement(By.Id(EMAIL_ID));
            AssertElementDisplayed(elementEmail);
            elementEmail.SendKeys("test@test.se");

            IWebElement elementPhoneNumber = driver.FindElement(By.Id(PHONE_NUMBER_ID));
            AssertElementDisplayed(elementPhoneNumber);
            elementPhoneNumber.SendKeys("555-555-1234");

            IWebElement elementCompany = driver.FindElement(By.Id(COMPANY_ID));
            AssertElementDisplayed(elementCompany);
            elementCompany.SendKeys("Test");
            elementCompany.Click();
            elementCompany.SendKeys(Keys.Escape);
            elementCompany.Click();
            elementCompany.SendKeys(Keys.Escape);

            IWebElement dropdownProductInterest = driver.FindElement(By.Id(PRODUCT_INTEREST));
            ClickFirstElementInDropdown(dropdownProductInterest, 1);

            IWebElement elementMessage = driver.FindElement(By.Id("Textarea-1"));
            AssertElementDisplayed(elementMessage);
            elementMessage.SendKeys("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");

            IWebElement dropdownCountry = driver.FindElement(By.Id(COUNTRY));
            ClickFirstElementInDropdown(dropdownCountry, 1);

            //agreeCheckBoxSelected();

            IWebElement dropdownState = driver.FindElement(By.Id(STATE_DROPDOWN));
            AssertElementDisplayed(dropdownState);

            ClickFirstElementInDropdown(dropdownState, 5);

            IWebElement elementContactUsButton = driver.FindElement(By.XPath(CONTACT_US_BUTTON));
            AssertElementDisplayed(elementContactUsButton);
            elementContactUsButton.Click();


            IWebElement confirmationMessage = driver.FindElement(By.Id(CONFIRMATION_MESSAGE));
            AssertElementDisplayed(confirmationMessage);

            Assert.AreEqual(driver.Url, "https://www.ipswitch.com/test-form-page");

            Thread.Sleep(10000);

        }

            public void acceptConsent()
        {
            IWebElement elementConsentButton = driver.FindElement(By.Id("onetrust-accept-btn-handler"));
            elementConsentButton.Click();
            Thread.Sleep(500);
        }
        public void agreeCheckBoxSelected()
        {
            IWebElement elementAgreeCheckBox = driver.FindElement(By.ClassName("js-i-agree-checkbox"));
            //scrollToelement(elementAgreeCheckBox);
            elementAgreeCheckBox = waitForElementClickable(elementAgreeCheckBox);
            elementAgreeCheckBox.Click();
            Thread.Sleep(500);
        }

        public void assertElementHasError(IWebElement element, String error)
        {
            IWebElement sibling = getSibling(element);
            Assert.NotNull(sibling);
            String errorMsg = sibling.Text;
            Assert.IsTrue(errorMsg.Contains(error));
        }

        public static IWebElement getSibling(IWebElement element)
        {
            return element.FindElement(By.XPath("following-sibling::*[1]"));
        }

        public void ClickFirstElementInDropdown(IWebElement dropdown, int index)
        {
            AssertElementDisplayed(dropdown);
            SelectElement selectElement = new SelectElement(dropdown);
            selectElement.SelectByIndex(index);
        }


        public void AssertElementDisplayed(IWebElement element)
        {
            Assert.IsTrue(element.Displayed);
        }

        //public void scrollToelement(IWebElement element)
        //{
        //    Actions actions = new Actions(driver);
        //    actions.MoveToElement(element);
        //    actions.Perform();
        //}

        public IWebElement waitForElementClickable(IWebElement element)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(element));
        }
    }
}
