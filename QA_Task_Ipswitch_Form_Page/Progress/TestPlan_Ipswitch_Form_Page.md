# Test Plan

### Testing a contact form on the Test Ipswitch Form Page
##### Prepared by: Slava Gagova
##### Date: 28.01.2021

#### Content:

#### 1. Introduction
#### 2. Test objectives
#### 3. Scope of testing
##### 3.1. Functionalities to be tested
##### 3.2. Functionalities not to be tested
#### 4. Testing strategy
#### 5. Deliverables
#### 6. Resources
##### 6.1. Human resources
##### 6.2. Hardware
##### 6.3. Environment requirements/ Tools
#### 7. Schedule
#### 8. Suspension criteria
#### 9. Exit Criteria


## 1. Introduction

The object of testing is the contact form page  on the Test Ipswitch Form page.

Our goal is to verify that the form fields are present and fulfill the functional requirements.

## 2. Test objectives

The purpose of these testing activities is to:

- Verify if contact form works according to the specifications.
- Identify and raise the defects in the form

## 3. Scope of testing

We will focus on main functionalities of the Test Ipswitch Form page

#### 3.1. Functionalities to be tested
- In order to be sent the form all the fields should be present
- All the required fields should be filled in according to the requirements
- A confirmation submission message appears when the form is sent

#### 3.2. Functionalities not to be tested

The checkbox that is shown for GDPR compliant countries is not in the scope.
Fields prefill is not in the scope.

## 4. Testing strategy

Exploratory testing will cover the main features and happy paths.

Testing approach will combine both manual and automaton testing as follows:

- Presence of all the required fields
- High level scenarios will be covered
- Some negative tests will be included for submission of the form

## 5. Deliverables

- Test cases
- Automated tests code / repository link
- A report for exploratory Testing
- High level test cases


## 6. Resources
#### 6.1. Human resources

- Slava Gagova Junior QA

#### 6.2. Hardware

- MacBook Pro, macOS - version: 10.15.7, Processor: 2.3 GHz, Core i5, RAM 16 GB

#### 6.3. Environment requirements/ Tools

- IDE:Visual Studio
- Test Automation Design and Execution: Selenium
- Browsers: Chrome, FireFox, Safari
- other: iOS device
- Bug Tracking Tool: Gitlab
- Test Case: Excel Table

## 7. Schedule
- Test plan - 1 day
- Exploratory testing and report - 1 hour
- Design of test cases - 3 hours
- Set up test automation environment - 1 day
- Execution of manual testing - 3 hours
- Log bugs - 2 hours
- Automated tests execution - 2 days

## 8. Suspension criteria:
- We stop testing if 50% of our tests fail
- The testing starts again when the development team fixes all the issues
## 9. Exit Criteria
- 100% of test scenarios are executed;
- 95% of first priority tests pass;
- 80% of second priority test pass;
- 60% of third priority test pass.
