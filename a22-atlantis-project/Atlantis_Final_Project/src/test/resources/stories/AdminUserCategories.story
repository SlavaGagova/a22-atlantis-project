Meta: @adminCategories

Narrative:
As an admin user I create, edit and delete categories

Scenario: Admin add new category

Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminCategories.Button element
And I navigate to Admin Categories page
And Click addCategory.Button element
And I navigate to Category New page
And TypeImage children food.png in chooseFile.Input field
And TypeRandom New_ in inputCategory.Field field
And Click addCategorySave.Button element
And I navigate to Admin Categories page
Then I can see logout.LogoutButton element

Scenario: Admin delete a category
Meta: @skip
Given Click navigation.SignIn element
When I navigate to Login page
And Type atlantis.final.project@gmail.com in login.Username field
And Type Alabal@123 in login.Password field
And Click login.LoginButton element
And I navigate to Navigation page
And Click settings.Button element
And Click adminCategories.Button element
And I navigate to Admin Categories page
And ClickFirst editCategory.Button element
And I navigate to Update Category page
And Click deleteCategory.Button element
And I navigate to Delete Category page
And Click deleteCategoryYes.Button element
And I navigate to Admin Categories page
Then I can see logout.LogoutButton element